vmcreate
========

Examples
--------

Run unattended installation by using [ubuntu-ks.cfg](./kickstart/ubuntu-ks.cfg) kickstart file

**Note:** These commands have to be executed from Host (hypervisor) OS

Start simple HTTP server

```plain
python3 -m http.server --bind 192.168.122.1 --directory examples/ 8080
```

Create new virtual machine ([test1.conf](./test1.conf)) and install Ubuntu 20.04 LTS

```plain
./vmcreate.sh -C examples/test1.conf
```

After installation try to login to newly installed Ubuntu 20.04 LTS as `root` user (password: `changeme`)

```plain
ssh root@192.168.122.101
```
