#!/bin/bash
# Author: Milos Buncic
# Creation Date: 2012/02/28
# Update Date: 2018/10/26
# Script name: vmcreate.sh
# Script version: 1.2.0
# Description: Provision new virtual machine on KVM/Xen
#
# Note:  Script requires configuration file (example configuration file is included => vmcreate.conf)
#
# Additional functionalities:
#    - Check working environment (basic check for binaries)
#    - Create new vdisk (qcow2)
#    - Attach/Detach new vdisk
#    - Attach/Detach new network interface
#    - Attach/Detach ISO image (CDROM) (VM must be ON)
#    - Three different live migration modes
#        SINGLE (migrates single virtual machine from local to remote host)
#        MULTIPLE (migrates list (vm.list file - one machine name per line) of virtual machines from local to remote host)
#        ALL (migrates ALL virtual machines from local to remote host)

###
### GLOBAL VARS
###

# Show status messages, 0=disable 1=enable
DEBUG=1

# Today's date
#DATE=$(date +%Y%m%d%H%M%S)

# Text colors and styles
RED="\033[31m"
GREEN="\033[32m"
YELLOW="\033[33m"
BLUE="\033[34m"
PURPLE="\033[35m"
BLACK="\033[30m"
BOLD="\033[1m"                # bold text
NONE="\033[0m"                # reset all



###
### FUNCTIONS
###

### Check VM state (running, shut-off...)
check_vmstate() {
    local VM_NAME=$1

    virsh list | grep ${VM_NAME} &> /dev/null
    if [[ $? -eq 0 ]]; then
        ## VM running
        #return 0    # exit status -> $?
        echo "0"
    else
        ## VM not running
        #return 1    # exit status -> $?
        echo "1"
    fi
}



### Create new vdisk
create_vdisk() {
    #local DEBUG=1
    local VDISK_NAME=$1
    local VDISK_SIZE=${2:-8G}

    ## OPTIONS
    local FILE_FORMAT="-f qcow2"
    local OPTIONS="-o preallocation=metadata"

    if [[ ! -e ${VDISK_NAME} ]]; then
        qemu-img create ${FILE_FORMAT} ${OPTIONS} ${VDISK_NAME} ${VDISK_SIZE} 2> /dev/null
        if [[ $? -eq 0 ]]; then
            if [[ ${DEBUG} -eq 1 ]]; then
                echo -e ${VDISK_NAME##*/}${GREEN}" successfully created"${NONE}"\n"
            fi
        else
            if [[ ${DEBUG} -eq 1 ]]; then
                echo -e ${RED}"Error: "${NONE}""${VDISK_NAME##*/}${RED}" could not be created"${NONE}"\n"
            fi
        fi
        chmod 600 ${VDISK_NAME} &> /dev/null
    else
        if [[ ${DEBUG} -eq 1 ]]; then
            echo -e ${VDISK_NAME##*/}${GREEN}" already exists"${NONE}"\n"
           fi
    fi
}



### Attach/Detach CDROM (iso image)
attach_detach_cdrom() {
    #local DEBUG=1
    local VM_NAME=$2
    local ISO=$3
    local VCDROM_DEV_NAME=$(virsh dumpxml ${VM_NAME} | sed -ne '/cdrom/,/<\/disk>/s/.*\(hd[a-z]\).*/\1/p' 2> /dev/null)

    ## OPTIONS
    local OPTIONS="--mode readonly --type cdrom"

    case "$1" in
        a|attach)
            virsh attach-disk ${VM_NAME} ${ISO} ${VCDROM_DEV_NAME} ${OPTIONS} &> /dev/null
            if [[ $? -eq 0 ]]; then
                if [[ ${DEBUG} -eq 1 ]]; then
                    echo -e ${ISO##*/}${GREEN}" successfully attached"${NONE}"\n"
                fi
            else
                if [[ ${DEBUG} -eq 1 ]]; then
                    echo -e ${RED}"Error: "${NONE}${ISO##*/}${RED}" could not be attached"${NONE}"\n"
                fi
            fi
        ;;
        d|detach)
            echo "<disk type='block' device='cdrom'>
            <driver name='qemu' type='raw'/>
            <target dev='${VCDROM_DEV_NAME}' bus='ide'/>
            <readonly/>
            </disk>" \
            > /tmp/cdrom.xml

            virsh update-device ${VM_NAME} /tmp/cdrom.xml &> /dev/null
            if [[ $? -eq 0 ]]; then
                if [[ ${DEBUG} -eq 1 ]]; then
                    echo -e ${ISO##*/}${GREEN}" successfully detached"${NONE}"\n"
                fi
            else
                if [[ ${DEBUG} -eq 1 ]]; then
                    echo -e ${RED}"Error: "${NONE}${ISO##*/}${RED}" could not be detached"${NONE}"\n"
                fi
            fi
            rm -rf /tmp/cdrom.xml &> /dev/null
        ;;
    esac
}



### Attach/Detach vDisk
attach_detach_vdisk() {
    #local DEBUG=1
    local VM_NAME=$2
    local VDISK_DEV_NAME=$3
    local VDISK=$4

    ## OPTIONS
    local DISK_TYPE=$(echo ${VDISK} | awk -F"/" '/\/dev\// { print $2 }')
    ## Check disk type - block or file based
    if [[ ${DISK_TYPE} == "dev" ]]; then
        local SUBDRIVER="--subdriver raw"
        local SOURCETYPE="--sourcetype block"
    else
        local SUBDRIVER="--subdriver qcow2"
        local SOURCETYPE="--sourcetype file"
    fi
    local PERSISTENT="--persistent"
    local OPTIONS="--cache none ${SUBDRIVER} ${SOURCETYPE} ${PERSISTENT}"

    case "$1" in
        a|attach)
            virsh attach-disk ${VM_NAME} ${VDISK} ${VDISK_DEV_NAME} ${OPTIONS} &> /dev/null
            if [[ $? -eq 0 ]]; then
                   if [[ ${DEBUG} -eq 1 ]]; then
                    echo -e ${VDISK##*/}${GREEN}" successfully attached"${NONE}"\n"
                fi
            else
                if [[ ${DEBUG} -eq 1 ]]; then
                    echo -e ${RED}"Error: "${NONE}${VDISK##*/}${RED}" could not be attached to "${NONE}${VM_NAME}"\n"
                   fi
            fi
        ;;
        d|detach)
            virsh detach-disk ${VM_NAME} ${VDISK_DEV_NAME} ${PERSISTENT} &> /dev/null
            if [[ $? -eq 0 ]]; then
                if [[ ${DEBUG} -eq 1 ]]; then
                    echo -e ${VDISK_DEV_NAME}${GREEN}" successfully detached"${NONE}"\n"

                    ## External functions - START
                    #1
                    if [[ $(check_vmstate ${VM_NAME}) -eq 0 ]]; then
                        if [[ ${DEBUG} -eq 1 ]]; then
                            echo -e ${VDISK_DEV_NAME}${GREEN}" vdisk will be permanently detached after shutdown"${NONE}"\n"
                        fi
                    fi

                    #2
                    ## External functions - END
                fi
            else
                   if [[ ${DEBUG} -eq 1 ]]; then
                    echo -e ${RED}"Error: "${NONE}${VDISK_DEV_NAME}${RED}" could not be detached"${NONE}"\n"
                fi
            fi
        ;;
    esac
}



### Attach/Detach Network interface
attach_detach_interface() {
    #local DEBUG=1
    local VM_NAME=$2
    local NETWORK=$3
    ## Remove # when you finish with TEST CODE section
    #local MAC_ADDRESS=$3

    ## OPTIONS
    ## Remove # when you finish with TEST CODE section
    #local MAC="--mac ${MAC_ADDRESS}"
    local PERSISTENT="--persistent"
    local NETWORK_TYPE="bridge"
    local OPTIONS="${NETWORK_TYPE} ${NETWORK} --model virtio"

    ## Remove this test when you finish with TEST CODE section
    # Test - existence of parameters
    if [[ -z ${VM_NAME} ]] || [[ -z ${NETWORK} ]]; then
        if [[ ${DEBUG} -eq 1 ]]; then
            echo -e ${RED}"Error: You missed some parameters!"${NONE}"\n"
        fi
        exit 1
    fi

    case "$1" in
        a|attach)
            virsh attach-interface ${VM_NAME} ${OPTIONS} ${PERSISTENT} &> /dev/null
            if [[ $? -eq 0 ]]; then
                if [[ ${DEBUG} -eq 1 ]]; then
                    echo -e ${VM_NAME}${GREEN}" is successfully attached to "${NONE}${NETWORK}${GREEN}" network"${NONE}"\n"
                fi
            else
                if [[ ${DEBUG} -eq 1 ]]; then
                    echo -e ${RED}"Error: "${NONE}${VM_NAME}${RED}" cannot be attached to "${NONE}${NETWORK}${RED}" network"${NONE}"\n"
                fi
            fi
        ;;
        d|detach)
            ## Extract MAC address from bridge interface - START
            INTERFACE_LIST=(`virsh dumpxml ${VM_NAME} | sed -ne '/bridge/,/<\/interface>/p' | egrep -o "br[0-9]*[0-9]|[0-9,a-f][0-9,a-f]:[0-9,a-f][0-9,a-f]:[0-9,a-f][0-9,a-f]:[0-9,a-f][0-9,a-f]:[0-9,a-f][0-9,a-f]:[0-9,a-f][0-9,a-f]" 2> /dev/null`)

            CHECK=$(echo ${INTERFACE_LIST[@]} | grep -o "${NETWORK}" | tail -n1 2> /dev/null)
            if [[ "${CHECK}" != "${NETWORK}" ]]; then
                if [[ ${DEBUG} -eq 1 ]]; then
                    echo -e ${NETWORK}${RED}" doesn't exist!"${NONE}"\n"
                    exit 1
                fi
            fi

            ## Local function of 'attach_detach_interface' function - START
            id() {
                local COUNT=${#INTERFACE_LIST[@]}
                local INDEX=0

                while [ "$INDEX" -lt "$COUNT" ]; do
                    echo -e "$INDEX ${INTERFACE_LIST[$INDEX]}"
                    let "INDEX++"
                done \
                | grep "$NETWORK" | cut -d' ' -f1 | tail -n1 2> /dev/null
            }
            ## Local function of 'attach_detach_interface' function - END

            TEST=$(id)

            local MAC_ADDRESS=$(echo ${INTERFACE_LIST[$((${TEST} - 1))]} 2> /dev/null)
            local MAC="--mac ${MAC_ADDRESS}"
            ## Extract MAC address from bridge interface - END

            virsh detach-interface ${VM_NAME} ${NETWORK_TYPE} ${MAC} ${PERSISTENT} &> /dev/null
            if [[ $? -eq 0 ]]; then
                if [[ ${DEBUG} -eq 1 ]]; then
                    echo -e ${NETWORK}${GREEN}" interface is successfully detached from "${NONE}${VM_NAME}"\n"

                    ## External functions - START
                    #1
                    if [[ $(check_vmstate ${VM_NAME}) -eq 0 ]]; then
                        if [[ ${DEBUG} -eq 1 ]]; then
                            echo -e ${NETWORK}${GREEN}" interface will be permanently detached after shutdown"${NONE}"\n"
                        fi
                    fi

                    #2
                    ## External functions - END

                fi
            else
                if [[ ${DEBUG} -eq 1 ]]; then
                    echo -e ${RED}"Error: "${NONE}${NETWORK}${RED}" interface cannot be detached from "${NONE}${VM_NAME}"\n"
                fi
            fi
        ;;
    esac
}



### Create virtual machine
create_vm() {
    #local DEBUG=1
    local VM_CONF=$1

    if [[ -f ${VM_CONF} ]]; then
        . ${VM_CONF}

        ## OPTIONS
        ## virt-install options
        case ${HYPERVISOR} in
            "kvm")
                XML_PATH="/etc/libvirt/qemu/${VM_NAME}.xml"
                VI_VIRT_TYPE="--hvm"
                VI_CONNECT="--connect=qemu:///system"
                VI_DISK="--disk path=${VDISK_NAME},bus=virtio,cache=none"
            ;;
            "xen")
                XML_PATH="/etc/libvirt/libxl/${VM_NAME}.xml"
                VI_VIRT_TYPE="--paravirt"
                VI_CONNECT="--connect=xen:/// --console pty,target_type=xen"
                VI_DISK="--disk path=${VDISK_NAME},bus=xen,cache=none"
            ;;
            *)
                XML_PATH="/etc/libvirt/qemu/${VM_NAME}.xml"
                VI_VIRT_TYPE="--hvm"
                VI_CONNECT="--connect=qemu:///system"
                VI_DISK="--disk path=${VDISK_NAME},bus=virtio,cache=none"
        esac

        VI_NAME="--name=${VM_NAME}"
        VI_GRAPHICS="--graphics vnc"
        VI_VCPUS="--vcpus=${CPU}"
        VI_RAM="--ram=${RAM}"

        ## Define CPU model and feature set
        if [[ -z ${CPU_MODEL} ]] || [[ ${CPU_MODEL} == "auto" ]]; then
            unset VI_CPU_MODEL
        else
            VI_CPU_MODEL="--cpu ${CPU_MODEL}"
        fi

        ## Graphical or Console OS installation
        ## If LOCATION doesn't exist, skip the OS installation process, and build a guest around an existing disk image
        if [[ -n ${LOCATION} ]]; then
            if [[ ${CONSOLE} == "no" ]]; then
                VI_LOCATION="--cdrom=${LOCATION}"
                VI_VIRT_TYPE="--hvm"
            elif [[ ${CONSOLE} == "yes" ]]; then
                VI_GRAPHICS="--graphics none"
                VI_LOCATION="--location=${LOCATION}"

                ## Define kernel console
                case ${HYPERVISOR} in
                  "kvm")
                    KERNEL_ARGS="console=tty0 console=ttyS0,115200n8 serial"
                  ;;
                  "xen")
                    KERNEL_ARGS="text console=com1 utf8 console=hvc0"
                  ;;
                  *)
                    KERNEL_ARGS="console=tty0 console=ttyS0,115200n8 serial"
                esac

                ## Define extra kernel args for various operating systems
                case ${DISTRO} in
                    "rhel"|"centos")
                        VI_KERNEL_ARGS="--extra-args=ks=${KS} ksdevice=eth0 ip=${IPADDR} netmask=${NETMASK} gateway=${GATEWAY} dns=${DNS} ${KERNEL_ARGS}"
                    ;;
                    "debian"|"ubuntu")
                        VI_KERNEL_ARGS="--extra-args=ks=${KS} netcfg/disable_dhcp=true netcfg/choose_interface=eth0 netcfg/get_ipaddress=${IPADDR} netcfg/get_netmask=${NETMASK} netcfg/get_gateway=${GATEWAY} netcfg/get_nameservers=${DNS} netcfg/get_hostname=${HOSTNAME} netcfg/get_domain=${DOMAIN} ${KERNEL_ARGS}"
                    ;;
                    *)
                        if [[ ${DEBUG} -eq 1 ]]; then
                            echo -e ${YELLOW}"Warning: "${NONE}${YELLOW}"DISTRO is empty or not defined, valid values are 'rhel', 'centos', 'debian' or 'ubuntu'!"${NONE}
                            echo -e ${YELLOW}"Continued with default kernel args"${NONE}
                        fi
                        VI_KERNEL_ARGS="--extra-args=${KERNEL_ARGS}"
                esac
            else
                if [[ ${DEBUG} -eq 1 ]]; then
                    echo -e ${RED}"Error: "${NONE}${RED}"CONSOLE="${CONSOLE}" invalid value, supported values are 'yes' or 'no'!"${NONE}"\n"
                fi
                exit 1
            fi
        else
            VI_IMPORT="--import"
        fi

        ## Pass cloud-init metadata to the VM
        if [[ ${CLOUD_INIT_ENABLED} == "yes" ]]; then
            VI_CLOUD_INIT_OPTIONS="--cloud-init"
            if [[ -n ${CLOUD_INIT_OPTIONS} ]]; then
                VI_CLOUD_INIT_OPTIONS="${VI_CLOUD_INIT_OPTIONS} ${CLOUD_INIT_OPTIONS}"
            fi
        fi

        ## If bridge isn't defined create virtual machine without network connectivity
        if [[ -n ${BRIDGE} ]]; then
            case ${HYPERVISOR} in
                "kvm")
                    VI_NETWORK="--network bridge=${BRIDGE},model=virtio"
                ;;
                "xen")
                    VI_NETWORK="--network bridge=${BRIDGE}"
                ;;
                *)
                    VI_NETWORK="--network bridge=${BRIDGE},model=virtio"
            esac
        else
            VI_NETWORK="--nonetworks"
        fi

        ## Show boot menu yes or no
        if [[ ${BOOT_MENU} == "yes" ]]; then
            VI_BOOT="--boot ${BOOT_OPTIONS},menu=on"
        elif [[ ${BOOT_MENU} == "no" ]]; then
            VI_BOOT="--boot ${BOOT_OPTIONS},menu=off"
        else
            if [[ ${DEBUG} -eq 1 ]]; then
                echo -e ${RED}"Error: "${NONE}${RED}"BOOT_MENU="${BOOT_MENU}" is not valid value, supported values are 'yes' or 'no'!"${NONE}"\n"
            fi
            exit 1
        fi

        VI_OS_TYPE="--os-type=${OS_TYPE}"
        VI_OS_VARIANT="--os-variant=${OS_VARIANT}"
    else
        if [[ ${DEBUG} -eq 1 ]]; then
            echo -e ${RED}"Error: "${NONE}${RED}${VM_CONF}" is not valid configuration file!"${NONE}"\n"
        fi
        exit 1
    fi

    ## External functions - START
    #1
    if [[ ${VDISK_CREATE} == "yes" ]]; then
        create_vdisk ${VDISK_NAME} ${VDISK_SIZE}
    fi

    #2
    ## External functions - END

    ## Create virtual machine
    if [[ -n "${VI_KERNEL_ARGS}" ]]; then
        virt-install \
        ${VI_CONNECT} \
        ${VI_NAME} \
        ${VI_VIRT_TYPE} \
        ${VI_GRAPHICS} \
        ${VI_BOOT} \
        ${VI_DISK} \
        ${VI_VCPUS} \
        ${VI_CPU_MODEL} \
        ${VI_RAM} \
        ${VI_LOCATION} \
        ${VI_IMPORT} \
        ${VI_NETWORK} \
        ${VI_CLOUD_INIT_OPTIONS} \
        ${VI_OS_TYPE} \
        ${VI_OS_VARIANT} \
        "${VI_KERNEL_ARGS}"
    else
        virt-install \
        ${VI_CONNECT} \
        ${VI_NAME} \
        ${VI_VIRT_TYPE} \
        ${VI_GRAPHICS} \
        ${VI_BOOT} \
        ${VI_DISK} \
        ${VI_VCPUS} \
        ${VI_CPU_MODEL} \
        ${VI_RAM} \
        ${VI_LOCATION} \
        ${VI_IMPORT} \
        ${VI_NETWORK} \
        ${VI_CLOUD_INIT_OPTIONS} \
        ${VI_OS_TYPE} \
        ${VI_OS_VARIANT}
    fi

    ## Modify xml configuration file after virtual machine creation
    if [[ $? -eq 0 ]]; then
        if [[ ${CLOCK} == "localtime" ]]; then
            virsh destroy ${VM_NAME} &> /dev/null
            sed -i "s%<clock offset='utc'/>%<clock offset='localtime'/>%" ${XML_PATH}
            virsh define ${XML_PATH} &> /dev/null
            virsh start ${VM_NAME} &> /dev/null

            if [[ -n ${LOCATION} ]] && [[ ${CONSOLE} == "no" ]]; then
                attach_detach_cdrom attach ${VM_NAME} ${LOCATION} &> /dev/null
            fi
        fi
    else
        if [[ ${DEBUG} -eq 1 ]]; then
            echo -e ${RED}"Error: "${NONE}${RED}${VM_CONF}" can't create virtual machine, please check configuration file!"${NONE}"\n"
        fi
    fi
}



### Migration in three different ways
vm_migration() {
    #local DEBUG=1
    local VM_NAME=$1
    local HYP_NAME="${@:2}"

    ## Migration special strings (ALL and MULTIPLE mode)
    ## Note: Change these values if you have a virtual machine with the same name
    local M_ALL="ALL"
    local M_MULTIPLE="vm.list"

    ## Local function of 'vm_migration' function - START
    ## Migration function
    migration() {
        local VM_LIST="$1"
        local HYP_NAME=("${@:2}")
        local HYP_NUM=$[${#HYP_NAME[@]}-1]

        ## OPTIONS
        ## virsh migrate options
        local OPTIONS="--persistent --undefinesource --verbose"
        local M_MODE="--live"

        local COUNT=0
        for VM in ${VM_LIST}; do
            local M_URI="qemu+ssh://${HYP_NAME[${COUNT}]}/system"

            virsh migrate ${M_MODE} ${OPTIONS} ${VM} ${M_URI} #&> /dev/null

            if [[ $? -eq 0 ]]; then
                if [[ ${DEBUG} -eq 1 ]]; then
                    echo -e ${VM}${GREEN}" - migration has been successfully completed!"${NONE}
                fi
            else
                if [[ ${DEBUG} -eq 1 ]]; then
                    echo -e ${VM}${RED}" - migration aborted, error occurred!"${NONE}
                fi
            fi

            if [[ ${COUNT} -eq ${HYP_NUM} ]]; then
                local COUNT=0
                continue
            fi
            let COUNT++
        done
    }
    ## Local function of 'vm_migration' function - END



    ## Three different migration modes
    ## 1 SINGLE (migrates single virtual machine from local to remote host)
    if [[ $(check_vmstate ${VM_NAME}) -eq 0 ]]; then

        if [[ ${DEBUG} -eq 1 ]]; then
            echo -e ${BLUE}"SINGLE mode started!"${NONE}
        fi

        migration ${VM_NAME} ${HYP_NAME}
        echo

    ## 2 MULTIPLE (migrates list (vm.list file) of virtual machines from local to remote host)
    elif [[ ${VM_NAME##*/} == "${M_MULTIPLE}" ]]; then
        local VM_LIST=$(cat ${VM_NAME})

        if [[ ${DEBUG} -eq 1 ]]; then
            echo -e ${BLUE}"MULTIPLE mode started!"${NONE}
        fi

        migration "${VM_LIST}" ${HYP_NAME}
        echo

    ## 3 ALL (migrates all virtual machines from local to remote host)
    elif [[ ${VM_NAME} == "${M_ALL}" ]]; then

        if [[ ${DEBUG} -eq 1 ]]; then
            echo -e ${BLUE}"ALL mode started!"${NONE}
        fi

        ## List of ALL virtual machines running on the localhost
        #local VM_LIST=$(virsh list | tail -n +3 | awk '{print $2}' | sed '/^$/d')
        local VM_LIST=$(virsh list --name)

        migration "${VM_LIST}" ${HYP_NAME}
        echo

    else
        if [[ ${DEBUG} -eq 1 ]]; then
            echo -e ${VM_NAME}${RED}" is not running or doesn't exist, aborting migration!"${NONE}"\n"
        fi
        exit 1
    fi
}



### Check working env for this script
check_env() {
    #local DEBUG=1
    local COMPONENTS=(
            [0]="virt-install"
            [1]="virsh"
            [2]="qemu-img"
                     )

    for CHECK in "${COMPONENTS[@]}"; do
        which ${CHECK} &> /dev/null
        if [[ $? -eq 0 ]]; then
            echo -e "${CHECK} installed"${GREEN}" [OK]"${NONE}
        else
            echo -e "${CHECK} not installed"${RED}" [fail]"${NONE}
        fi
    done
    echo
}



### Script usage - HELP
help() {
    echo -e "
${BLUE}USAGE:
  ${GREEN}`basename $0` options${NONE} \n
${BLUE}OPTIONS:${NONE}
  -t                                                      Check working environment
  -C configuration_file_path                              Provision new virtual machine
  -q vdisk_file_path [vdisk_size]                         Create new vdisk
  -c attach|detach vm_name iso_image_path                 Attach/Detach CDROM (iso image)
  -d attach|detach vm_name vdisk_name vdisk_path          Attach/Detach additional vdisk
  -i attach|detach vm_name network_name                   Attach/Detach network interface
  -M vmname|filename|special_string host_name             Live migration
  -h                                                      display this help \n
${BLUE}EXAMPLES:${NONE}
  - Check working environment \n
    ${GREEN}vmcreate.sh -t${NONE} \n
  - Create new virtual machine from configuration file (vmcreate.conf)
    ${BLUE}Note:${NONE} Before provisioning VM from configuration file first make required changes in vmcreate.conf file \n
    ${GREEN}vmcreate.sh -C vmcreate.conf${NONE} \n
  - Create new qcow2 vdisk (default size - 8G) \n
    ${GREEN}vmcreate.sh -q /var/lib/libvirt/images/vdiskname.img${NONE}
    or with defined size
    ${GREEN}vmcreate.sh -q /var/lib/libvirt/images/vdiskname.img 16G${NONE} \n
  - Attach and Detach CDROM (iso image) \n
    Attach:
    ${GREEN}vmcreate.sh -c attach vmname /kvm/ISO/OS.iso${NONE}
    or
    ${GREEN}vmcreate.sh -c a vmname /kvm/ISO/OS.iso${NONE} \n
    Detach:
    ${GREEN}vmcreate.sh -c detach vmname${NONE}
    or
    ${GREEN}vmcreate.sh -c d vmname${NONE} \n
  - Attach and Detach additionl vdisk \n
    Attach:
    ${GREEN}vmcreate.sh -d attach vmname vdb /var/lib/libvirt/images/vdiskname2.img${NONE}
    or
    ${GREEN}vmcreate.sh -d a vmname vdb /var/lib/libvirt/images/vdiskname2.img${NONE} \n
    Detach:
    ${GREEN}vmcreate.sh -d detach vmname vdb /var/lib/libvirt/images/vdiskname2.img${NONE}
    or
    ${GREEN}vmcreate.sh -d d vmname vdb${NONE} \n
  - Attach and Detach network interface \n
    Attach:
    ${GREEN}vmcreate.sh -i attach vmname br0${NONE}
    or
    ${GREEN}vmcreate.sh -i a vmname br0${NONE} \n
    Detach:
    ${GREEN}vmcreate.sh -i detach vmname br0${NONE}
    or
    ${GREEN}vmcreate.sh -i d vmname br0${NONE} \n
  - Three different live migration modes \n
    ${BLUE}SINGLE${NONE} (migrates single virtual machine from local to remote host): \n
    ${GREEN}vmcreate.sh -M vmname hypervisorname${NONE} \n
    ${BLUE}MULTIPLE${NONE} (migrates list (vm.list file - one machine name per line) of virtual machines from local to remote host): \n
    ${GREEN}vmcreate.sh -M vm.list hypervisorname${NONE} \n
    ${BLUE}ALL${NONE} (migrates ALL virtual machines from local to remote host): \n
    ${GREEN}vmcreate.sh -M ALL hypervisorname${NONE}
"
}

### Check if command line arguments exist
if [[ $# -eq 0 ]]; then
    if [[ ${DEBUG} -eq 1 ]]; then
        echo -e ${RED}"Error: "${NONE}${RED}"Script requires at least one argument!"${NONE}
    fi
    help
    exit 1
fi



###
### ARGUMENT DEFINITIONS
###

while getopts ":C:M:q:c:d:i:th" OPT; do
    case ${OPT} in
        C)
            echo -e "Creating new virtual machine..."
            shift
            create_vm $1
        ;;
        M)
            echo -e "Starting migration..."
            shift
            vm_migration $1 "${@:2}"
        ;;
        q)
            echo -e "Creating vdisk..."
            shift
            create_vdisk $1 $2
        ;;
        c)
            echo -e "Attaching/Detaching CDROM...""\n"
            shift
            VMSTATE=$(check_vmstate $2)

            if [[ ${VMSTATE} -ne 0 ]]; then
                if [[ ${DEBUG} -eq 1 ]]; then
                    echo -e ${GREEN}"$2 "${RED}"is down or doesn't exist!""\n"${NONE}
                fi
                exit 1
            fi

            attach_detach_cdrom $1 $2 $3
        ;;
        d)
            echo -e "Attaching/Detaching vdisk...""\n"
            shift
            attach_detach_vdisk $1 $2 $3 $4
        ;;
        i)
            echo -e "Attaching/Detaching network interface...""\n"
            shift
            attach_detach_interface $1 $2 $3
        ;;
        t)
            echo -e "Checking environment...""\n"
            sleep 1
            check_env
        ;;
        h)
            help
        ;;
        \?)
            if [[ ${DEBUG} -eq 1 ]]; then
                echo -e ${RED}"Error: "${NONE}${RED}"Invalid option: -${OPTARG}"${NONE} >&2
            fi
            help
            exit 1
        ;;
        \:)
            if [[ ${DEBUG} -eq 1 ]]; then
                echo -e ${RED}"Error: "${NONE}${RED}"Option -${OPTARG} requires an argument"${NONE} >&2
            fi
            help
            exit 1
        ;;
    esac
done
