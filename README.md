vmcreate
========

About
-----

Simple script for provisioning and managing virtual machines and resources via libvirt virtualization API

Usage
-----

Show help

```plain
vmcreate.sh -h
```

Check working environment

```plain
vmcreate.sh -t
```

Create new virtual machine from configuration file vmcreate.conf

**Note:** First you have to edit conf file and then you can run command

```plain
vmcreate.sh -C vmcreate.conf
```

Create new qcow2 vdisk (default size - 8G)

```plain
vmcreate.sh -q /var/lib/libvirt/images/vdiskname.img
```

or with defined size

```plain
vmcreate.sh -q /var/lib/libvirt/images/vdiskname.img 16G
```

Attach and Detach CDROM (ISO image)

Attach:

```plain
vmcreate.sh -c attach vmname /kvm/ISO/OS.iso
```

or

```plain
vmcreate.sh -c a vmname /kvm/ISO/OS.iso
```

Detach:

```plain
vmcreate.sh -c detach vmname
```

or

```plain
vmcreate.sh -c d vmname
```

Attach and Detach additionl vdisk

Attach:

```plain
vmcreate.sh -d attach vmname vdb /var/lib/libvirt/images/vdiskname2.img
```

or

```plain
vmcreate.sh -d a vmname vdb /var/lib/libvirt/images/vdiskname2.img
```

Detach:

```plain
vmcreate.sh -d detach vmname vdb /var/lib/libvirt/images/vdiskname2.img
```

or

```plain
vmcreate.sh -d d vmname vdb
```

Attach and Detach network interface

Attach:

```plain
vmcreate.sh -i attach vmname br0
```

or

```plain
vmcreate.sh -i a vmname br0
```

Detach:

```plain
vmcreate.sh -i detach vmname br0
```

or

```plain
vmcreate.sh -i d vmname br0
```

Three different live migration modes

**SINGLE** (migrates single virtual machine from local to remote host):

```plain
vmcreate.sh -M vmname hypervisorname
```

**MULTIPLE** (migrates list (vm.list file - one machine name per line) of virtual machines from local to remote host):

```plain
vmcreate.sh -M vm.list hypervisorname
```

**ALL** (migrates ALL virtual machines from local to remote host):

```plain
vmcreate.sh -M ALL hypervisorname
```

Misc
----

We need to configure `ttyS0` and `tty0` in order to enable proper console access via virsh command

**Note:** These commands have to be executed from VM OS

```plain
systemctl enable serial-getty@ttyS0.service
systemctl start serial-getty@ttyS0.service
```

```plain
sed -i 's/GRUB_CMDLINE_LINUX=""/GRUB_CMDLINE_LINUX="console=ttyS0,115200n8 console=tty0"/g' /etc/default/grub
update-grub
```

Reboot VM OS after making all required changes

```plain
reboot
```

Now you can try to access VM via `virsh console`

**Note:** This command have to be executed from the Host (hypervisor) OS

```plain
virsh console vmname
```

**Note:** Don't forget to hit **Enter** one more time after executing `virsh console` command
